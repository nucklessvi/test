#!/bin/bash

# Update system
sudo pacman -Syu --noconfirm

# Install minimal GNOME
sudo pacman -S --noconfirm gnome-console gnome-control-center gnome-shell gnome-tweaks loupe nautilus xdg-desktop-portal-gnome

# Install GNOME Display Manager
sudo pacman -S --noconfirm gdm

# Install fonts
sudo pacman -S --noconfirm noto-fonts noto-fonts-cjk noto-fonts-emoji noto-fonts-extra

# Install additional packages
sudo pacman -S --noconfirm chromium ffmpeg micro mpv nicotine+ p7zip transmission wine winetricks yt-dlp